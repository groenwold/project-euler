import timeit

multiples_limiter = {3,5}
lower_limit = 1
upper_limit = 1000

def find_multiples_range(range_start, range_end, multiples):
    found_multiples = set()
    for i in range(range_start, range_end):
        for j in multiples:
            if i % j == 0:
                found_multiples.add(i)
    return found_multiples

if __name__ == '__main__':
    #TODO add timing
    multiples = find_multiples_range(lower_limit, upper_limit, multiples_limiter)
    print(sum(multiples))



# Multiples of 3 and 5
# Problem 1
#
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.
